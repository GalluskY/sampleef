This project is a sample on how to use the Entity Framework for .NET applications with a MySQL database.

Before trying to run the project you have to follow the steps below : 
1. In your phpMyAdmin, run the script in the "Database" folder
2. Donwload the MySQL connector at this link => https://dev.mysql.com/downloads/connector/net/8.0.html
3. Donwload the MySQL plugin for Visual Studio at this link => https://dev.mysql.com/downloads/file/?id=478113
4. Open Visual Studio, click "Server Explorer" under the Display tab in the tool bar
5. In the Server Explorer, right click on "Data connexion" and then click on "Add a connexion"
6. Select "MySQL provider" for the data provider and go to next step
7. Fill the form with the information of your server and user using "classicshopdb" for the name of the database
8. Test the connexion

You can now run the solution and run it. glhf.