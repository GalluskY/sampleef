﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 * In this program, the entity for the classicshopdb database is already created.
 * However, if you wanna create a new entity, follow the steps below : 
 * - Right click on your project in the solution explorer
 * - Add>New Item
 * - Select ADO.NET Entity Data Model
 * - In the new dialog, select EF Model From Database
 * - Then, fill the dialog's steps as you wish your new entity to be
 */
namespace SampleEF
{
    class Program
    {
        static void Main(string[] args)
        {
            // This class type is defined by the name you give to an entity when you create it
            // It regroups all the tables from your database as class with relations
            EntityClassicShopDB context = new EntityClassicShopDB();
            string cmd = string.Empty; 
            
            while (!cmd.Equals("exit"))
            {
                Console.WriteLine("Enter the SQL command you wanna try (select/insert/update/delete) or type exit to exit the sample program");
                Console.WriteLine("You should use the classic workflow for this sample to work properly select -> insert -> update -> delete");
                Console.WriteLine(string.Format("Last command executed is : {0}", cmd.Equals(string.Empty) ? "No command executed yet" : cmd));
                cmd = Console.ReadLine();

                switch (cmd)
                {
                    case "select":
                        // SELECT sample
                        SelectSample(context);
                        break;
                    case "insert":
                        // INSERT sample
                        InsertSample(context);
                        break;
                    case "delete":
                        // DELETE sample
                        DeleteSample(context);
                        break;
                    case "update":
                        // UPDATE sample
                        UpdateSample(context);
                        break;
                }
            }
        }

        static void SelectSample(EntityClassicShopDB context)
        {
            // This is how to do a select
            // from is defining an alias for a table from the context of your database
            // in is assigning a table from the context of your database to the alias defined with the from
            // select defines what column to select from the table set before with the in
            var req = (from c in context.products select c).ToList();

            // You can then access the request result just like if it was objects
            foreach (var line in req)
                Console.WriteLine(string.Format("{0} | {1} | {2} ", line.ProductID, line.ProductName, line.categories.CategoryName));

            Console.WriteLine("\r\nPress <<ENTER>> to continue");
        }

        static void InsertSample(EntityClassicShopDB context)
        {
            Console.WriteLine("Before..");
            SelectSample(context);

            try
            {
                // To add a row in a table, you just have to instantiate an object of the type of the table from your entity
                // There is no constructor so you must use the syntax below to define its properties
                // Ortherwise you can set them one by one after instantiation 
                var product = new products()
                {
                    ProductName = "Sample Product",
                    SupplierID = 1,
                    CategoryID = 1,
                    Unit = "Sample Unit"
                };

                // Add it to your table in your ENTITY
                context.products.Add(product);
                // Don't forget to commit the changes in your entity in your database
                context.SaveChanges();

                Console.WriteLine("After..");
                SelectSample(context);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        static void DeleteSample(EntityClassicShopDB context)
        {
            // Retrieve the row you wanna delete from your database
            var toDelete = context.products.First(p => p.ProductID == (from x in context.products select x.ProductID).ToList().Max());
            Console.WriteLine("Before..");
            SelectSample(context);

            try
            {
                // Remove it from your ENTITY
                context.products.Remove(toDelete);
                // Commit the changes to your database
                context.SaveChanges();

                Console.WriteLine("Afeter..");
                SelectSample(context);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        static void UpdateSample(EntityClassicShopDB context)
        {
            // Select the row to update from your database
            var toUpdate = context.products.First(p => p.ProductID == (from x in context.products select x.ProductID).ToList().Max());
            Console.WriteLine("Before..");
            SelectSample(context);

            try
            {
                // Update whatever you want in your entity
                toUpdate.ProductName += " Updated";
                // Commit the updated row in your database
                context.SaveChanges();

                Console.WriteLine("Afeter..");
                SelectSample(context);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
